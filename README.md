# Best resource in CS (Spring'19)

标签（空格分隔）： MOOC

---

[TOC]

> Up-to-date best lectures in CS (Jan'19)

# Courses
## Coding
* UCB cs61a
    * Textbook: [Composing Programms](http://composingprograms.com/)
    * Ported SICP from Lisp to Python3!
* UCB cs61b
    * [lec notes Fall'18](http://www-inst.eecs.berkeley.edu/~cs61b/fa18/)

## Algs
* UCB cs170 : pair with Algorithms (DPV) and youtube video
* UCB IAP puzzles : like Prof. Srini Devadas. In Python.

## AI
* UCB cs188 : by famous Prof, an intro class
    * [lec and youtube video](https://inst.eecs.berkeley.edu/~cs188/fa18/)

## Discrete Math & probability
* UCB cs70
    * [only lec note](http://www.eecs70.org/)

## Database
* CMU 15-445: Fall'18 video in youtube, book: Database Concept 6th
    * [lec note and Youtube video](https://15445.courses.cs.cmu.edu/fall2018/) 
    * lec 7 Tree Index 1: Warning! BAD  AUDIO!!!

* optional: UCB 186: need School email to register for Edx Edge so as to enroll. I used Gatech and successed. 
    * Based on low attendance in prior semesters, we are experimenting with online delivery for CS186 this fall. 哈哈哈.

## Distributed System
* MIT 6.824
 	* [lec note](https://pdos.csail.mit.edu/6.824/index.html)

* U Washington cse452
    * same project as 6.824, no video
    * [Good lec note](https://courses.cs.washington.edu/courses/cse452/16wi/calendar/sectionlist.html)

* Waterloo cs436 
    * [youtube video link](http://blizzard.cs.uwaterloo.ca/keshav/wiki/index.php/Videos)
    * based on Distributed Systems 2nd.

* [UMass cs677 link](http://lass.cs.umass.edu/~shenoy/courses/677)
    * [Video on youtube](https://www.youtube.com/watch?v=utqXvPmIu3M&list=PLacuG5pysFbAtw3al_0cNjg6zGOs54ETw&index=30) 
    * book: [Distributed Systems 3rd 2017](https://www.distributed-systems.net/)
    * Always go for lecture note as 1st resource. Since it's re-ordered, not same as book.
        

---

# Books
## DB
* new Database book by @ifesdjeen [twitter announcement](https://twitter.com/ifesdjeen/status/1078664460573831169)
    ![ifesdjeen_DBbook.png](img/ifesdjeen_DBbook.png)
* db-book (Database System Concepts, 7th. Feb 12, 2019)

## Distributed System
* [DSTEP from Prof. Remzi](http://pages.cs.wisc.edu/~remzi/DSTEP/)
* Distributed Systems 3rd (v3.0.2) : [link](https://www.distributed-systems.net/index.php/books/distributed-systems-3rd-edition-2017/)
* Vasia Kalavri @vkalavri: [Stream Processing with ApacheFlink](https://twitter.com/vkalavri/status/1080783299549843456). Will release on Apr'19.
    * ![stream_processing_with_ApacheFlink.png](img/stream_processing_with_ApacheFlink.png) 

## Algs
* Jeff Erickson的Algorithms : algorithms.wtf
